import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Admin pages
import UserProfile from 'src/components/Dashboard/Views/UserProfile.vue'
import Overview from 'src/components/Dashboard/Views/Painel/Overview.vue'
import Login from 'src/components/Dashboard/Views/Painel/Login.vue'
import Alunos from 'src/components/Dashboard/Views/Alunos/Alunos.vue'
import Funcionarios from 'src/components/Dashboard/Views/Funcionarios/Funcionarios.vue'
import Cursos from 'src/components/Dashboard/Views/Cursos/Cursos.vue'
import Livros from 'src/components/Dashboard/Views/Livros/Livros.vue'
import Revistas from 'src/components/Dashboard/Views/Revistas/Revistas.vue'
import Jornais from 'src/components/Dashboard/Views/Jornais/Jornais.vue'
import Midias from 'src/components/Dashboard/Views/Midias/Midias.vue'
import Anais from 'src/components/Dashboard/Views/Anais/Anais.vue'
import TCCS from 'src/components/Dashboard/Views/TCCS/TCCS.vue'


import EditarAluno from '../components/Dashboard/Views/Alunos/EditComponent.vue'
import EditarFuncionario from '../components/Dashboard/Views/Funcionarios/EditComponent.vue'
import EditarCurso from '../components/Dashboard/Views/Cursos/EditComponent.vue'
import EditarLivro from '../components/Dashboard/Views/Livros/EditComponent.vue'
import EditarRevista from '../components/Dashboard/Views/Revistas/EditComponent.vue'
import EditarJornal from '../components/Dashboard/Views/Jornais/EditComponent.vue'
import EditarMidia from '../components/Dashboard/Views/Midias/EditComponent.vue'
import EditarAnal from '../components/Dashboard/Views/Anais/EditComponent.vue'
import EditarTCC from '../components/Dashboard/Views/TCCS/EditComponent.vue'



import Vue from "vue"
import VueRouter from "vue-router"

import LightBootstrap from "../light-bootstrap-main"
import store from "../store"
import * as types from "../store/mutation-types"

const hasToken = (to, from, next) => {
  const token = localStorage.getItem('JWT')
  const username = localStorage.getItem('username')
  if (token) {
    store.commit(types.LOGIN_SUCCESS, { token, username })
    router.push('/ufab')
  } else {
    next()
  }
}

const requireAuth = (to, from, next) => {
  if (store.getters.isLoggedIn) {
    next()
  } else {
    router.push('/')
  }
}

Vue.use(VueRouter)
Vue.use(LightBootstrap)

const index = [
  {
    path: '/',
    alias: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: hasToken
  },
  {
    path: '/ufab',
    component: DashboardLayout,
    redirect: '/ufab/dashboard',
    beforeEnter: requireAuth,
    children: [
      {
        path: 'dashboard',
        name: 'Overview',
        component: Overview
      },
      {
        path: 'user',
        name: 'User',
        component: UserProfile
      },
      {
        path: 'alunos',
        name: 'Alunos',
        component: Alunos,
      },
      { path: 'alunos/edit/:id', name:'EditarAluno', component: EditarAluno },
      {
        path: 'funcionarios',
        name: 'Funcionarios',
        component: Funcionarios
      },
      { path: 'funcionarios/edit/:id', name:'EditarFuncionario', component: EditarFuncionario },
      {
        path: 'cursos',
        name: 'Cursos',
        component: Cursos
      },
      { path: 'cursos/edit/:id', name:'EditarCurso', component: EditarCurso },
      {
        path: 'livros',
        name: 'Livros',
        component: Livros
      },
      { path: 'livros/edit/:id', name:'EditarLivro', component: EditarLivro },
      {
        path: 'revistas',
        name: 'Revistas',
        component: Revistas
      },
      { path: 'revistas/edit/:id', name:'EditarRevista', component: EditarRevista },
      {
        path: 'jornais',
        name: 'Jornais',
        component: Jornais
      },
      { path: 'jornais/edit/:id', name:'EditarJornal', component: EditarJornal },
      {
        path: 'midias',
        name: 'midias',
        component: Midias
      },
      { path: 'midias/edit/:id', name:'EditarMidia', component: EditarMidia },
      {
        path: 'anais',
        name: 'anais',
        component: Anais
      },
      { path: 'anais/edit/:id', name:'EditarAnal', component: EditarAnal },
      {
        path: 'tccs',
        name: 'tccs',
        component: TCCS
      },
      { path: 'tccs/edit/:id', name:'EditarTCC', component: EditarTCC },

    ]
  },
  { path: '*', component: NotFound }
];

const router = new VueRouter({routes: index});


export default router
