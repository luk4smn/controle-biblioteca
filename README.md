## Controle de Biblioteca

Sistema que permitira o controle de emprestimos de livro


## Estrutura do sistema

- Abaixo temos o Diagrama de Dom�nios das Classes:

![picture](DiagramaUML.png)

## Instrucoes


### Banco de Dados
###### - Ao executar o sistema será criado um banco de dados com o nome controle_biblioteca
###### - Alguns dados já serão inseridos automaticamente
###### - As configurações de acesso para esse banco de dados estão em:
```bash
 controle-biblioteca/src/main/resources/application.properties
```

### Iniciando

### Para subir o Back-end na porta 8080:
##### Navegue pelo terminal até a pasta do projeto e execute:
```bash
 mvn spring-boot:run
```
#####- Ou pelo eclipse
 ```bash
  Run as -> Spring Boot App 
```

### Para subir o Front-End na porta 8000:
##### Navegue pelo terminal até a pasta do projeto e depois até a pasta frontend
###### - feito isso, execute:
```bash
 npm install
```
###### - após executar o passo acima execute :
```bash
 npm run dev
```