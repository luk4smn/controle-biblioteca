package com.uepb.controlebiblioteca.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
* Jornal representa os itens do tipo Jornal que poder�o ser cadastrados no sistema.
* @author Lucas Nunes
*
*/
@Entity
@Table(name = "JORNAIS")
public class Jornal implements Serializable {

    private static final long serialVersionUID = -3465813074586302847L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // Variavel inteira id do livro, gerando automaticamente.

    @Column
    private String titulo; // Variavel string editora do livro.

    @Column
    private int edicao; // Variavel inteira edicao do livro.

    @Column
    private Date publicacao; // Vari�vel Date onde ser� atribuido o ano de nascimento do aluno.


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public int getEdicao() {
        return edicao;
    }

    public void setEdicao(int edicao) {
        this.edicao = edicao;
    }


    public Date getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(Date publicacao) {
        this.publicacao = publicacao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}