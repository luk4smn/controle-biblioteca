package com.uepb.controlebiblioteca.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Revistas representa a estrutura do objeto revista 
 * que poder� ser manipulada no sistema por um usuario com autoriza��o.
 * @author Eduardo Borba
 *
 */
@Entity
@Table(name = "REVISTAS")
public class Revista implements Serializable {

	private static final long serialVersionUID = 4L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; // variavel inteira id da revista.
	
	@Column
	private String editora; // variavel string da editora da revista.

	@Column
	private int edicao;// variavel inteira da edicao da revista.
	
	@Column
	private int numPaginas;// variavel inteira do numero de paginas da revista.

	@Column
	private String titulo; // Variavel string area de conhecimento do livro.

	@Column
	private Date publicacao; // Vari�vel Date onde ser� atribuido o ano de nascimento do aluno.

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public int getEdicao() {
		return edicao;
	}

	public void setEdicao(int edicao) {
		this.edicao = edicao;
	}

	public int getNumPaginas() {
		return numPaginas;
	}

	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}

	public String getTitulo() {
		return titulo;
	}

	public Date getPublicacao() {
		return publicacao;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setPublicacao(Date publicacao) {
		this.publicacao = publicacao;
	}
}