package com.uepb.controlebiblioteca.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * AppUser representa qualquer instancia de usuario do sistema.
 * 
 * @author Eduardo Borba
 *
 */
@Entity
public class AppUser implements Serializable {

	private static final long serialVersionUID = 8L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; // Vari�vel inteira de id do Usuario, � gerada automaticamente.
	
	@Column(nullable = false, unique = true)
	private String username;  // Vari�vel String do nome do Usuario. Obrigatoriedades:  deve ser unica e n�o nula.
	
	@Column(nullable = false) // Vari�vel String de senha do Usuario, Obrigatoriedades: n�o nula
	private String password;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<UserRole> roles; // Variavel das roles do Usuario, varios usuarios podem ter varias roles.

	public AppUser() {
	}
	
	/**
	 * Constroi e inicializa um Usuario, com os atributos - Nome, senha e role.
	 * @param username
	 * @param password
	 * @param roles
	 */
	public AppUser(String username, String password, List<UserRole> roles) {
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
