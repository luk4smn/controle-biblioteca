package com.uepb.controlebiblioteca.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ANAIS_CONGRESSO")
/**
 * AnaisCongresso representa .
 * @author Eduardo Borba
 *
 */
public class AnaisCongresso implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; // Vari�vel inteira de id de um item anaisCongresso, � gerada automaticamente.
	
	@Column
	private String tipo; // Vari�vel String referente ao tipo de anaisCongresso.

	@Column
	private String nomeCongresso; // Vari�vel String referente ao nome desse anaisCongresso.

	@Column
	private String local; // Vari�vel String referente ao local de origem desse anaisCongresso.

	@Column
	private String titulo;

	@Column
	private String autores;

	@Column
	private Date publicacao;

	public AnaisCongresso() {
		
	}

	/**
	 * Constroi e inicializa um AnalCongresso com os atributos - id, tipo, nomeCongresso, local.
	 * @param id
	 * @param tipo
	 * @param nomeCongresso
	 * @param local
	 */
	public AnaisCongresso(Long id, String tipo, String nomeCongresso, String local) {
		this.id = id;
		this.tipo = tipo;
		this.nomeCongresso = nomeCongresso;
		this.local = local;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNomeCongresso() {
		return nomeCongresso;
	}

	public void setNomeCongresso(String nomeCongresso) {
		this.nomeCongresso = nomeCongresso;
	}

	public String getLocal() {
		return local;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getAutores() {
		return autores;
	}

	public Date getPublicacao() {
		return publicacao;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setAutores(String autores) {
		this.autores = autores;
	}

	public void setPublicacao(Date publicacao) {
		this.publicacao = publicacao;
	}

	public void setLocal(String local) {
		this.local = local;
	}
}