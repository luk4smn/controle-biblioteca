package com.uepb.controlebiblioteca.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * UserRole representa a definicao da role a qual o usuario pertence.
 * Define o n�vel de acesso do usuario.
 * @author Eduardo Borba
 *
 */
@Entity
public class UserRole implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -7428336891195151331L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;// variavel inteira id da userRole.
	
    @Column(nullable = false, unique = true)
    private String roleName; // variavel string do nome da role. N�o nula e �nica.

    public UserRole() {
    }

    public UserRole(String roleName) {
        this.roleName = roleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
