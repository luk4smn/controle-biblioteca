package com.uepb.controlebiblioteca.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TrabalhosConclusao representa a estrutura do objeto trabalho de conclusao 
 * que poder� ser manipulada no sistema por um usuario com autoriza��o.
 * @author Eduardo Borba
 *
 */
@Entity
@Table(name = "TRABALHOS_CONCLUSAO")
public class TrabalhosConclusao implements Serializable {

	private static final long serialVersionUID = 5L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; // variavel inteira id do trabalho de conclusao.
	
	@Column
	private String tipo;  // variavel string tipo do trabalho de conclusao.

	@Column
	private String local;

	@Column
	private String titulo;

	@Column
	private String autores;

	@Column
	private String orientadores;

	@Column
	private Date dataDefesa;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getAutores() {
		return autores;
	}

	public String getOrientadores() {
		return orientadores;
	}

	public Date getDataDefesa() {
		return dataDefesa;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setAutores(String autores) {
		this.autores = autores;
	}

	public void setOrientadores(String orientadores) {
		this.orientadores = orientadores;
	}

	public void setDataDefesa(Date dataDefesa) {
		this.dataDefesa = dataDefesa;
	}
}