/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.dao;

import org.springframework.data.repository.CrudRepository;

import com.uepb.controlebiblioteca.model.Reserva;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Interface responsavel pela definicao dos metodos com possibilidade de
 *         uso.
 **/
public interface ReservaDao extends CrudRepository<Reserva, Long> {

}