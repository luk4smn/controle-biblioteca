/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.dao;

import com.uepb.controlebiblioteca.model.Curso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.uepb.controlebiblioteca.model.Aluno;
import org.springframework.data.repository.query.Param;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Interface responsavel pela definicao dos metodos com possibilidade de
 *         uso.
 **/
public interface AlunoDao extends CrudRepository<Aluno, Long> {

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Aluno c WHERE c.curso = :curso")
    boolean existsAlunoByCurso(@Param("curso") Curso curso);

}