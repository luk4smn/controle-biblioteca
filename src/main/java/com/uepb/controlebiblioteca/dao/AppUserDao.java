/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uepb.controlebiblioteca.model.AppUser;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Interface responsavel pela definicao dos metodos com possibilidade de
 *         uso.
 **/
public interface AppUserDao extends JpaRepository<AppUser, Long> {

	@Query("SELECT c FROM AppUser c WHERE c.username = :name")
	AppUser findByName(@Param("name") String name);
}