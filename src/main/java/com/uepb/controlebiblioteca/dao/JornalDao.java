package com.uepb.controlebiblioteca.dao;

import com.uepb.controlebiblioteca.model.Jornal;
import org.springframework.data.repository.CrudRepository;

/***
*
* @author Lucas Martins, Lanmark Rafael, Higor Pereira
*
*         Interface responsavel pela definicao dos metodos com possibilidade de
*         uso.
**/
public interface JornalDao extends CrudRepository<Jornal, Long> {
}
