/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uepb.controlebiblioteca.model.UserRole;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Interface responsavel pela definicao dos metodos com possibilidade de
 *         uso.
 **/
public interface UserRoleDao extends JpaRepository<UserRole, Long> {

	@Query("SELECT c FROM UserRole c WHERE c.roleName = :name")
	UserRole findByName(@Param("name") String name);
}