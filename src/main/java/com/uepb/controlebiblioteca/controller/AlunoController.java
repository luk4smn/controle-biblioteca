/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */

package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.AlunoDao;
import com.uepb.controlebiblioteca.dao.AppUserDao;
import com.uepb.controlebiblioteca.dao.UserRoleDao;
import com.uepb.controlebiblioteca.model.Aluno;
import com.uepb.controlebiblioteca.model.AppUser;
import com.uepb.controlebiblioteca.model.UserRole;
import com.uepb.controlebiblioteca.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel pelos servicos de controle do crud dos alunos.
 **/
@RestController
@RequestMapping("${spring.data.rest.base-path}/alunos")
public class AlunoController {

	/**
	 * Define a interface alunoRepository para essa classe.
	 */
	@Autowired
	private AlunoDao alunoRepository;

	@Autowired
	private AppUserDao appUserRepository;

	@Autowired
	private UserRoleDao userRoleRepository;

	/**
	 * rota [/alunos], e retorna um json com o modelo definido [carregar todos os
	 * dados de alunos].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<Aluno>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<Aluno>>(alunoRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<Aluno>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/alunos/id], e retorna um json com o modelo definido [carregar um dos
	 * dados de aluno]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Aluno>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<Aluno>>(alunoRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<Aluno>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a um aluno.
	 *
	 * @param aluno
	 *            - aluno como parametro de comparacao, para saber se ira adicionar
	 *            um novo aluno ou se ira apenas atualizar algum existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody Aluno aluno, UriComponentsBuilder ucBuilder) {

		alunoRepository.save(aluno);

		if(aluno.getMatricula() == null){
			aluno.gerarMatricula();
			alunoRepository.save(aluno);

			UserRole role = userRoleRepository.findByName("ALUNO");
			List<UserRole> roles = new ArrayList<>();
			roles.add(role);
			appUserRepository.save((new AppUser(aluno.getMatricula(), Util.encodePassword(aluno.getSenha()), roles)));
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/alunos/{id}").buildAndExpand(aluno.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody Aluno aluno) {
		if (aluno == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		alunoRepository.save(aluno);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta o aluno.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id do aluno passado, e deleta o
	 *            aluno referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<Aluno>> delete(@PathVariable("id") Long id, Aluno aluno) {
		Optional<Aluno> list = alunoRepository.findById(id);

		if (list.isPresent())
			aluno = list.get();

		if (aluno == null) {
			return new ResponseEntity<Optional<Aluno>>(HttpStatus.NOT_FOUND);
		}

		alunoRepository.deleteById(id);

		return new ResponseEntity<Optional<Aluno>>(HttpStatus.NO_CONTENT);
	}
}