/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.TrabalhosConclusaoDao;
import com.uepb.controlebiblioteca.model.TrabalhosConclusao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos do crud de TCCs.
 **/

@RestController
@RequestMapping("${spring.data.rest.base-path}/tccs")
public class TrabalhosConclusaoController {

	/**
	 * Define a interface tccRepository para essa classe.
	 */
	@Autowired
	private TrabalhosConclusaoDao tccRepository;

	/**
	 * rota [/tccs], e retorna um json com o modelo definido [carregar todos os
	 * dados de tccs].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<TrabalhosConclusao>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<TrabalhosConclusao>>(tccRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<TrabalhosConclusao>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/tccs/id/get], e retorna um json com o modelo definido [carregar um dos
	 * dados de tccs]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<TrabalhosConclusao>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<TrabalhosConclusao>>(tccRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<TrabalhosConclusao>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a um tcc.
	 *
	 * @param tcc
	 *            - tcc como parametro de comparacao, para saber se ira adicionar um
	 *            novo tcc ou se ira apenas atualizar algum existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody TrabalhosConclusao tcc, UriComponentsBuilder ucBuilder) {
		tccRepository.save(tcc);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/tccs/{id}").buildAndExpand(tcc.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody TrabalhosConclusao tcc) {
		if (tcc == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		tccRepository.save(tcc);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta o tcc.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id do tcc passado, e deleta o tcc
	 *            referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<TrabalhosConclusao>> delete(@PathVariable("id") Long id, TrabalhosConclusao tcc) {
		Optional<TrabalhosConclusao> list = tccRepository.findById(id);

		if (list.isPresent())
			tcc = list.get();

		if (tcc == null) {
			return new ResponseEntity<Optional<TrabalhosConclusao>>(HttpStatus.NOT_FOUND);
		}

		tccRepository.deleteById(id);

		return new ResponseEntity<Optional<TrabalhosConclusao>>(HttpStatus.NO_CONTENT);
	}
}