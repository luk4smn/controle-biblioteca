/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */

package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.AppUserDao;
import com.uepb.controlebiblioteca.dao.FuncionarioDao;
import com.uepb.controlebiblioteca.dao.UserRoleDao;
import com.uepb.controlebiblioteca.model.AppUser;
import com.uepb.controlebiblioteca.model.Funcionario;
import com.uepb.controlebiblioteca.model.UserRole;
import com.uepb.controlebiblioteca.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos de crud de
 *         Funcionarios.
 **/
@RestController
@RequestMapping("${spring.data.rest.base-path}/funcionarios")
public class FuncionarioController {

	/**
	 * Define a interface funcionarioRepository para essa classe.
	 */
	@Autowired
	private FuncionarioDao funcionarioRepository;

	@Autowired
	private AppUserDao appUserRepository;

	@Autowired
	private UserRoleDao userRoleRepository;

	/**
	 * rota [/funcionario], e retorna um json com o modelo definido [carregar todos
	 * os dados de funcionarios].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<Funcionario>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<Funcionario>>(funcionarioRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<Funcionario>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/funcionarios/id], e retorna um json com o modelo definido [carregar um
	 * dos dados de funcionarios]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Funcionario>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<Funcionario>>(funcionarioRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<Funcionario>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a um funcionario.
	 *
	 * @param funcionario
	 *            - funcionario como parametro de comparacao, para saber se ira
	 *            adicionar um novo funcionario ou se ira apenas atualizar algum
	 *            existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody Funcionario funcionario, UriComponentsBuilder ucBuilder) {
		funcionarioRepository.save(funcionario);

		if(funcionario.getId() == null){
			UserRole role = userRoleRepository.findByName("FUNCIONARIO");
			List<UserRole> roles = new ArrayList<>();
			roles.add(role);
			appUserRepository
					.save((new AppUser(funcionario.getNome(), Util.encodePassword(funcionario.getSenha()), roles)));
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/funcionarios/{id}").buildAndExpand(funcionario.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody Funcionario funcionario) {
		if (funcionario == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		funcionarioRepository.save(funcionario);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta o funcionario.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id do funcionario passado, e
	 *            deleta o funcionario referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<Funcionario>> delete(@PathVariable("id") Long id, Funcionario funcionario) {
		Optional<Funcionario> list = funcionarioRepository.findById(id);

		if (list.isPresent())
			funcionario = list.get();

		if (funcionario == null) {
			return new ResponseEntity<Optional<Funcionario>>(HttpStatus.NOT_FOUND);
		}

		funcionarioRepository.deleteById(id);

		return new ResponseEntity<Optional<Funcionario>>(HttpStatus.NO_CONTENT);
	}
}