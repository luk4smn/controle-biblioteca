/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */

package com.uepb.controlebiblioteca.controller;

import java.io.IOException;
import java.util.Optional;

import com.uepb.controlebiblioteca.dao.EmprestimoDao;
import com.uepb.controlebiblioteca.model.Emprestimo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos de controle de
 *         emprestimo.
 **/

@RestController
@RequestMapping("${spring.data.rest.base-path}/emprestimos")
public class EmprestimoController {

	/**
	 * Define a interface emprestimoRepository para essa classe.
	 */
	@Autowired
	private EmprestimoDao emprestimoRepository;

	/**
	 * rota [/emprestimos], e retorna um json com o modelo definido [carregar todos
	 * os dados de emprestimos].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<Emprestimo>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<Emprestimo>>(emprestimoRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<Emprestimo>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/emprestimos/id], e retorna um json com o modelo definido [carregar um
	 * dos dados de emprestimos]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Emprestimo>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<Emprestimo>>(emprestimoRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<Emprestimo>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a um emprestimo.
	 *
	 * @param emprestimo
	 *            - emprestimo como parametro de comparacao, para saber se ira
	 *            adicionar um novo emprestimo ou se ira apenas atualizar algum
	 *            existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody Emprestimo emprestimo, UriComponentsBuilder ucBuilder) {
		emprestimoRepository.save(emprestimo);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/cursos/{id}").buildAndExpand(emprestimo.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody Emprestimo emprestimo) {
		if (emprestimo == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		emprestimoRepository.save(emprestimo);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta o emprestimo.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id do emprestimo passado, e deleta
	 *            o emprestimo referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<Emprestimo>> delete(@PathVariable("id") Long id, Emprestimo emprestimo) {
		Optional<Emprestimo> list = emprestimoRepository.findById(id);

		if (list.isPresent())
			emprestimo = list.get();

		if (emprestimo == null) {
			return new ResponseEntity<Optional<Emprestimo>>(HttpStatus.NOT_FOUND);
		}

		emprestimoRepository.deleteById(id);

		return new ResponseEntity<Optional<Emprestimo>>(HttpStatus.NO_CONTENT);
	}
}