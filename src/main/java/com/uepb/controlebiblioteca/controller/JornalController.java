package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.JornalDao;
import com.uepb.controlebiblioteca.model.Jornal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Optional;

/***
 *
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos do crud de Jornais.
 **/

@RestController
@RequestMapping("${spring.data.rest.base-path}/jornais")
public class JornalController {
    /**
     * Define a interface jornalRepository para essa classe.
     */
    @Autowired
    private JornalDao jornalRepository;

    /**
     * rota [/jornais], e retorna
     * um json com o modelo definido [carregar todos os dados de jornais].
     * @throws IOException - tratamento de excecao.
     */
    @GetMapping()
    public ResponseEntity<Iterable<Jornal>> listAll() throws IOException
    {
        try {
            return new ResponseEntity<Iterable<Jornal>>(jornalRepository.findAll(), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Iterable<Jornal>>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * rota [/id], e retorna
     * um json com o modelo definido [carregar um dos dados de jornal]
     * @param id
     * @throws IOException - tratamento de excecao.
     */
    @GetMapping (value = "/{id}")
    public ResponseEntity<Optional<Jornal>> getOne(@PathVariable("id") Long id) throws IOException
    {
        try {
            return new ResponseEntity<Optional<Jornal>>(jornalRepository.findById(id), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Optional<Jornal>>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * Salva dados referente a um jornal.
     *
     * @param jornal - jornal como parametro de comparacao, para saber
     * se ira adicionar uma nova jornal ou se ira apenas atualizar algum existente.
     *
     */
    @PostMapping()
    public ResponseEntity<Void> save(@RequestBody Jornal jornal, UriComponentsBuilder ucBuilder)
    {
        jornalRepository.save(jornal);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/jornais/{id}").buildAndExpand(jornal.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<Void> update(@RequestBody Jornal jornal)
    {
        if (jornal == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        jornalRepository.save(jornal);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    /**
     * Deleta o jornal.
     * @param id - usa como parametro de busca o id
     * da jornal passado, e deleta o jornal referente.
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Optional<Jornal>> delete(@PathVariable("id") Long id, Jornal jornal)
    {
        Optional<Jornal> list = jornalRepository.findById(id);

        if(list.isPresent()) jornal = list.get();

        if (jornal == null) {
            return new ResponseEntity<Optional<Jornal>>(HttpStatus.NOT_FOUND);
        }

        jornalRepository.deleteById(id);

        return new ResponseEntity<Optional<Jornal>>(HttpStatus.NO_CONTENT);
    }
}
