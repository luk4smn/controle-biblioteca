/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.LivroDao;
import com.uepb.controlebiblioteca.model.Livro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos de crud de Livros.
 **/

@RestController
@RequestMapping("${spring.data.rest.base-path}/livros")
public class LivroController {

	/**
	 * Define a interface livroRepository para essa classe.
	 */
	@Autowired
	private LivroDao livroRepository;

	/**
	 * rota [/livros], e retorna um json com o modelo definido [carregar todos os
	 * dados de livros].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<Livro>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<Livro>>(livroRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<Livro>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/livros/id], e retorna um json com o modelo definido [carregar um dos
	 * dados de livro]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Livro>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<Livro>>(livroRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<Livro>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a um livro.
	 *
	 * @param livro
	 *            - livro como parametro de comparacao, para saber se ira adicionar
	 *            um novo livro ou se ira apenas atualizar algum existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody Livro livro, UriComponentsBuilder ucBuilder) {
		livroRepository.save(livro);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/livros/{id}").buildAndExpand(livro.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody Livro livro) {
		if (livro == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		livroRepository.save(livro);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta o livro.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id do livro passado, e deleta o
	 *            livro referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<Livro>> delete(@PathVariable("id") Long id, Livro livro) {
		Optional<Livro> list = livroRepository.findById(id);

		if (list.isPresent())
			livro = list.get();

		if (livro == null) {
			return new ResponseEntity<Optional<Livro>>(HttpStatus.NOT_FOUND);
		}

		livroRepository.deleteById(id);

		return new ResponseEntity<Optional<Livro>>(HttpStatus.NO_CONTENT);
	}
}