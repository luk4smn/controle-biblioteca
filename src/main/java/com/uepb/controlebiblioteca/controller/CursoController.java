/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */

package com.uepb.controlebiblioteca.controller;

import java.io.IOException;
import java.util.Optional;

import com.uepb.controlebiblioteca.dao.AlunoDao;
import com.uepb.controlebiblioteca.dao.CursoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.uepb.controlebiblioteca.model.Curso;
import org.springframework.web.util.UriComponentsBuilder;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos de controle do crud de
 *         Cursos;
 **/

@RestController
@RequestMapping("${spring.data.rest.base-path}/cursos")
public class CursoController {

	/**
	 * Define a interface cursoRepository para essa classe.
	 */
	@Autowired
	private CursoDao cursoRepository;

	@Autowired
	private AlunoDao alunoRepository;


	/**
	 * rota [/cursos], e retorna um json com o modelo definido [carregar todos os
	 * dados de cursos].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<Curso>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<Curso>>(cursoRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<Curso>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/cursos/id], e retorna um json com o modelo definido [carregar um dos
	 * dados de cursos]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Curso>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<Curso>>(cursoRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<Curso>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a um curso.
	 * 
	 * @param curso
	 *            - curso como parametro de comparacao, para saber se ira adicionar
	 *            um novo curso ou se ira apenas atualizar algum existente.
	 *
	 * @return - retorna a view /cursos.
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody Curso curso, UriComponentsBuilder ucBuilder) {
		cursoRepository.save(curso);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/cursos/{id}").buildAndExpand(curso.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody Curso curso) {
		if (curso == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		cursoRepository.save(curso);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta o curso.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id do curso passado, e deleta o
	 *            curso referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<Curso>> delete(@PathVariable("id") Long id, Curso curso) {
		Optional<Curso> list = cursoRepository.findById(id);

		if (list.isPresent())
			curso = list.get();

		if (curso == null) {
			return new ResponseEntity<Optional<Curso>>(HttpStatus.NOT_FOUND);
		}

		boolean test = alunoRepository.existsAlunoByCurso(curso);

		if(!test){
			cursoRepository.deleteById(id);
			return new ResponseEntity<Optional<Curso>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<Optional<Curso>>(HttpStatus.NOT_MODIFIED);


	}
}