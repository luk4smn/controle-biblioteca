/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.ReservaDao;
import com.uepb.controlebiblioteca.model.Reserva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos de Controle de
 *         Reservas.
 **/
@RestController
@RequestMapping("${spring.data.rest.base-path}/reservas")
public class ReservaController {

	/**
	 * Define a interface reservaRepository para essa classe.
	 */
	@Autowired
	private ReservaDao reservaRepository;

	/**
	 * rota [/reservas], e retorna um json com o modelo definido [carregar todos os
	 * dados de reservas].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<Reserva>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<Reserva>>(reservaRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<Reserva>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/reservas/id/get], e retorna um json com o modelo definido [carregar um
	 * dos dados de reserva]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Reserva>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<Reserva>>(reservaRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<Reserva>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a uma reserva.
	 *
	 * @param reserva
	 *            - reserva como parametro de comparacao, para saber se ira
	 *            adicionar uma nova reserva ou se ira apenas atualizar algum
	 *            existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody Reserva reserva, UriComponentsBuilder ucBuilder) {
		reservaRepository.save(reserva);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/reserva/{id}").buildAndExpand(reserva.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody Reserva reserva) {
		if (reserva == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		reservaRepository.save(reserva);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta a reserva.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id da reserva passado, e deleta o
	 *            reserva referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<Reserva>> delete(@PathVariable("id") Long id, Reserva reserva) {
		Optional<Reserva> list = reservaRepository.findById(id);

		if (list.isPresent())
			reserva = list.get();

		if (reserva == null) {
			return new ResponseEntity<Optional<Reserva>>(HttpStatus.NOT_FOUND);
		}

		reservaRepository.deleteById(id);

		return new ResponseEntity<Optional<Reserva>>(HttpStatus.NO_CONTENT);
	}
}