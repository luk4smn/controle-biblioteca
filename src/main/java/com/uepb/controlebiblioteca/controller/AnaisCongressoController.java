/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */

package com.uepb.controlebiblioteca.controller;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.uepb.controlebiblioteca.dao.AnaisCongressoDao;
import com.uepb.controlebiblioteca.model.AnaisCongresso;
import org.springframework.web.util.UriComponentsBuilder;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos de controle do crud
 *         dos Anais de Congresso.
 **/

@RestController
@RequestMapping("${spring.data.rest.base-path}/anais")
public class AnaisCongressoController {

	/**
	 * Define a interface anaisCongressoRepository para essa classe.
	 */
	@Autowired
	private AnaisCongressoDao anaisCongressoRepository;

	/**
	 * rota [/anais], e retorna um json com o modelo definido [carregar todos os
	 * dados de anais].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<AnaisCongresso>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<AnaisCongresso>>(anaisCongressoRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<AnaisCongresso>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/anais/id], e retorna um json com o modelo definido [carregar um dos
	 * dados de anais]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<AnaisCongresso>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<AnaisCongresso>>(anaisCongressoRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<AnaisCongresso>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a um anal.
	 *
	 * @param anal
	 *            - anais como parametro de comparacao, para saber se ira adicionar
	 *            um novo anal ou se ira apenas atualizar algum existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody AnaisCongresso anal, UriComponentsBuilder ucBuilder) {
		anaisCongressoRepository.save(anal);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/anais/{id}").buildAndExpand(anal.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody AnaisCongresso anal) {
		if (anal == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		anaisCongressoRepository.save(anal);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta o anal.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id do anal passado, e deleta o
	 *            anal referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<AnaisCongresso>> delete(@PathVariable("id") Long id, AnaisCongresso anal) {
		Optional<AnaisCongresso> list = anaisCongressoRepository.findById(id);

		if (list.isPresent())
			anal = list.get();

		if (anal == null) {
			return new ResponseEntity<Optional<AnaisCongresso>>(HttpStatus.NOT_FOUND);
		}

		anaisCongressoRepository.deleteById(id);

		return new ResponseEntity<Optional<AnaisCongresso>>(HttpStatus.NO_CONTENT);
	}
}