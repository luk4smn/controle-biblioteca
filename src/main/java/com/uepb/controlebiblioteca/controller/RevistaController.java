/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.RevistaDao;
import com.uepb.controlebiblioteca.model.Revista;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos do crud de Revistas.
 **/

@RestController
@RequestMapping("${spring.data.rest.base-path}/revistas")
public class RevistaController {

	/**
	 * Define a interface revistaRepository para essa classe.
	 */
	@Autowired
	private RevistaDao revistaRepository;

	/**
	 * rota [/revistas], e retorna
	 * um json com o modelo definido [carregar todos os dados de revistas].
	 * @throws IOException - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<Revista>> listAll() throws IOException
	{
		try {
			return new ResponseEntity<Iterable<Revista>>(revistaRepository.findAll(), HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<Iterable<Revista>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/revistas/id/get], e retorna
	 * um json com o modelo definido [carregar um dos dados de revista]
	 * @param id
	 * @throws IOException - tratamento de excecao.
	 */
	@GetMapping (value = "/{id}")
	public ResponseEntity<Optional<Revista>> getOne(@PathVariable("id") Long id) throws IOException
	{
		try {
			return new ResponseEntity<Optional<Revista>>(revistaRepository.findById(id), HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<Optional<Revista>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save].
	 * Salva dados referente a uma revista.
	 *
	 * @param revista - revista como parametro de comparacao, para saber
	 * se ira adicionar uma nova revista ou se ira apenas atualizar algum existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody Revista revista, UriComponentsBuilder ucBuilder)
	{
		revistaRepository.save(revista);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/revistas/{id}").buildAndExpand(revista.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody Revista revista)
	{
		if (revista == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		revistaRepository.save(revista);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete].
	 * Deleta a revista.
	 * @param id - usa como parametro de busca o id
	 * da revista passado, e deleta o revista referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<Revista>> delete(@PathVariable("id") Long id, Revista revista)
	{
		Optional<Revista> list = revistaRepository.findById(id);

		if(list.isPresent()) revista = list.get();

		if (revista == null) {
			return new ResponseEntity<Optional<Revista>>(HttpStatus.NOT_FOUND);
		}

		revistaRepository.deleteById(id);

		return new ResponseEntity<Optional<Revista>>(HttpStatus.NO_CONTENT);
	}
}