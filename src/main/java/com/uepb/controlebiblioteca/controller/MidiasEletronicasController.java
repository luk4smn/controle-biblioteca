/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.controller;

import com.uepb.controlebiblioteca.dao.MidiasEletronicasDao;
import com.uepb.controlebiblioteca.model.MidiasEletronicas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo responsavel por representar os servicos de crud de Midias
 *         Eletronicas.
 **/
@RestController
@RequestMapping("${spring.data.rest.base-path}/midias")
public class MidiasEletronicasController {

	/**
	 * Define a interface midiasRepository para essa classe.
	 */
	@Autowired
	private MidiasEletronicasDao midiasRepository;

	/**
	 * rota [/midias], e retorna um json com o modelo definido [carregar todos os
	 * dados de midias].
	 * 
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping()
	public ResponseEntity<Iterable<MidiasEletronicas>> listAll() throws IOException {
		try {
			return new ResponseEntity<Iterable<MidiasEletronicas>>(midiasRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Iterable<MidiasEletronicas>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota [/midias/id], e retorna um json com o modelo definido [carregar um dos
	 * dados de midias]
	 * 
	 * @param id
	 * @throws IOException
	 *             - tratamento de excecao.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<MidiasEletronicas>> getOne(@PathVariable("id") Long id) throws IOException {
		try {
			return new ResponseEntity<Optional<MidiasEletronicas>>(midiasRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Optional<MidiasEletronicas>>(HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * rota: [/save]. Salva dados referente a uma midia.
	 *
	 * @param midia
	 *            - midia como parametro de comparacao, para saber se ira adicionar
	 *            um novo midia ou se ira apenas atualizar algum existente.
	 *
	 */
	@PostMapping()
	public ResponseEntity<Void> save(@RequestBody MidiasEletronicas midia, UriComponentsBuilder ucBuilder) {
		midiasRepository.save(midia);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/midias/{id}").buildAndExpand(midia.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<Void> update(@RequestBody MidiasEletronicas midias) {
		if (midias == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}

		midiasRepository.save(midias);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * rota: [/delete]. Deleta a midia.
	 * 
	 * @param id
	 *            - usa como parametro de busca o id da midia passado, e deleta o
	 *            midia referente.
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Optional<MidiasEletronicas>> delete(@PathVariable("id") Long id, MidiasEletronicas midia) {
		Optional<MidiasEletronicas> list = midiasRepository.findById(id);

		if (list.isPresent())
			midia = list.get();

		if (midia == null) {
			return new ResponseEntity<Optional<MidiasEletronicas>>(HttpStatus.NOT_FOUND);
		}

		midiasRepository.deleteById(id);

		return new ResponseEntity<Optional<MidiasEletronicas>>(HttpStatus.NO_CONTENT);
	}
}