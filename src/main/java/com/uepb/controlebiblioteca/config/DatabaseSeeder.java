/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */

package com.uepb.controlebiblioteca.config;

import com.uepb.controlebiblioteca.dao.*;
import com.uepb.controlebiblioteca.model.*;
import com.uepb.controlebiblioteca.util.Util;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Metodo que semeia o banco de dados.
 **/

@Component
public class DatabaseSeeder {

	private static final Logger logger = Logger.getLogger(DatabaseSeeder.class);

	@Autowired
	private AppUserDao appUserRepository;

	@Autowired
	private UserRoleDao userRoleRepository;

	@Autowired
	private AlunoDao alunoRepository;

	@Autowired
	private FuncionarioDao funcionarioRepository;

	@Autowired
	private CursoDao cursoRepository;

	@EventListener
	public void seed(ContextRefreshedEvent event) {
		seedUserRoleTable();
		seedUsersTable();
		seedCursosTable();
		seedAlunosTable();
		seedFuncionariosTable();
	}

	private void seedUserRoleTable() {
		long qtdRoles = userRoleRepository.count();

		if (qtdRoles <= 0) {
			userRoleRepository.save((new UserRole("ADMIN")));
			userRoleRepository.save((new UserRole("FUNCIONARIO")));
			userRoleRepository.save((new UserRole("ALUNO")));

			logger.info("Roles Seeded");
		} else {
			logger.info("Roles Seeding Not Required");
		}
	}

	private void seedUsersTable() {
		long qtdUsers = appUserRepository.count();

		if (qtdUsers <= 0) {

			UserRole role = userRoleRepository.findByName("ADMIN");
			List<UserRole> roles = new ArrayList<>();
			roles.add(role);
			appUserRepository.save((new AppUser("root@ufab.com", Util.encodePassword("root"), roles)));

			logger.info("Users Seeded");
		} else {
			logger.info("Users Seeding Not Required");
		}
	}

	private void seedCursosTable() {
		long qtdCursos = cursoRepository.count();

		if (qtdCursos <= 0) {

			Curso curso = new Curso();

			curso.setArea("Ciências Exatas");
			curso.setNivel("Graduação");
			curso.setNome("Ciência da Computação");
			curso.setSigla("CC");

			cursoRepository.save(curso);

			logger.info("Cursos Seeded");
		} else {
			logger.info("Cursos Seeding Not Required");
		}
	}

	private void seedAlunosTable() {
		long qtdAlunos = alunoRepository.count();

		if (qtdAlunos <= 0) {
			Curso curso = null;

			Optional<Curso> list = cursoRepository.findById(new Long(1));

			if (list.isPresent())
				curso = list.get();

			Aluno aluno = new Aluno();

			aluno.setAno(java.sql.Date.valueOf("2018-05-20"));
			aluno.setCpf("026.189.845-01");
			aluno.setCurso(curso);
			aluno.setEndereco("Rua a, 35");
			aluno.setNaturalidade("Jacobina");
			aluno.setNome("Lucas Martins Nunes");
			aluno.setMae("Jucileide C. Martins");
			aluno.setPai("José Emanuel Neris Nunes");
			aluno.setPeriodo("1");
			aluno.setSenha("0123");
			aluno.setRg("1560211539");
			alunoRepository.save(aluno);

			aluno.gerarMatricula();
			alunoRepository.save(aluno);

			UserRole role = userRoleRepository.findByName("ALUNO");
			List<UserRole> roles = new ArrayList<>();
			roles.add(role);
			appUserRepository.save((new AppUser(aluno.getMatricula(), Util.encodePassword(aluno.getSenha()), roles)));

			logger.info("Alunos Seeded");
		} else {
			logger.info("Alunos Seeding Not Required");
		}
	}

	private void seedFuncionariosTable() {
		long qtdFuncionarios = funcionarioRepository.count();

		if (qtdFuncionarios <= 0) {

			Funcionario funcionario = new Funcionario();

			funcionario.setCpf("026.189.845-01");
			funcionario.setEmail("maria@ufab.com");
			funcionario.setEndereco("Rua a, 36");
			funcionario.setNaturalidade("Campina Grande");
			funcionario.setTelefone("83 98606-5885");
			funcionario.setSenha("0123");
			funcionario.setNome("Maria Pereira");

			funcionarioRepository.save(funcionario);

			UserRole role = userRoleRepository.findByName("FUNCIONARIO");
			List<UserRole> roles = new ArrayList<>();
			roles.add(role);
			appUserRepository.save((new AppUser(funcionario.getEmail(), Util.encodePassword("0123"), roles)));

			logger.info("Funcionarios Seeded");
		} else {
			logger.info("Funcionario Seeding Not Required");
		}
	}
}