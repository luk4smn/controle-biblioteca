/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Representacao de uma classe com um funcionalidade de model and view.
 **/

public class RedirectManager {
	/**
	 * ModelAndView retorna um modelo com uma view baseada nos parametros passados,
	 * model e view.
	 * 
	 * @param model
	 * @param view
	 * 
	 */
	public ModelAndView userDetail(ModelAndView model, String view) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName(); // get logged in username

		model.addObject("username", name);
		model.setViewName(view);
		return model;
	}

	public ModelAndView redirectToView(ModelAndView model, String view) {
		model.setViewName(view);
		return model;
	}

	public RedirectView redirectToUrl(String url) {
		return new RedirectView(url);
	}
}