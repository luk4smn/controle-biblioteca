/* 
 * Componente Curricular: Programacao WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 4
 */
package com.uepb.controlebiblioteca.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 *         Representacao de ferramenta com funcionadade de encriptacao da senha
 *         informada.
 **/

public class Util {
	/**
	 * encodePassword retorna um password criptografado, usando como parametro um
	 * String password passado pelo usuario.
	 * 
	 * @param password
	 * 
	 */
	public static String encodePassword(String password) {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(password);
	}
}